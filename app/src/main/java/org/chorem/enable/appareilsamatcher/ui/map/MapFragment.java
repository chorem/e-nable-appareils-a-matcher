/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui.map;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.chorem.enable.appareilsamatcher.R;
import org.chorem.enable.appareilsamatcher.databinding.FragmentMapBinding;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.ui.MatchRequestsViewModel;
import org.chorem.enable.appareilsamatcher.utils.CanRefreshData;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.CopyrightOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.TilesOverlay;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class MapFragment extends Fragment implements CanRefreshData {

    private FragmentMapBinding binding;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMapBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.swipeRefresh.setEnabled(false);

        Context context = requireContext();
        org.osmdroid.config.Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));

        MapView map = binding.osmmap;
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setExpectedCenter(new GeoPoint(46.0, 3.0));
        map.setMultiTouchControls(true);

        int nightModeFlags = getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;
        if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {
            TilesOverlay mapOverlay = map.getMapOverlay();
            mapOverlay.setColorFilter(TilesOverlay.INVERT_COLORS);
            mapOverlay.setLoadingBackgroundColor(R.color.black);
            mapOverlay.setLoadingLineColor(Color.argb(255, 0, 255, 0));
        }

        List<Overlay> overlays = map.getOverlays();

        IMapController controller = map.getController();
        controller.setZoom(6.0);

        CopyrightOverlay copyright = new CopyrightOverlay(context);
        copyright.setAlignBottom(true);
        overlays.add(copyright);

        MatchRequestsViewModel mapViewModel = new ViewModelProvider(this).get(MatchRequestsViewModel.class);
        mapViewModel.getAllMatchRequests().observe(getViewLifecycleOwner(), allMatchRequests -> {
            List<Overlay> markers = overlays.stream()
                    .filter(overlay -> overlay instanceof Marker)
                    .collect(Collectors.toList());
            overlays.removeAll(markers);

            allMatchRequests.forEach(matchRequest -> map.getOverlays().add(new MapMarker(map, matchRequest)));

            OptionalDouble minLat = allMatchRequests.stream().mapToDouble(MatchRequest::getLatitude).min();
            OptionalDouble maxLat = allMatchRequests.stream().mapToDouble(MatchRequest::getLatitude).max();
            OptionalDouble minLng = allMatchRequests.stream().mapToDouble(MatchRequest::getLongitude).min();
            OptionalDouble maxLng = allMatchRequests.stream().mapToDouble(MatchRequest::getLongitude).max();

            BoundingBox boundingBox = new BoundingBox();
            boundingBox.set(
                    maxLat.orElse(51d) + 5.0,
                    maxLng.orElse(8d) + 5.0,
                    minLat.orElse(42d) - 2.0,
                    minLng.orElse(-5d) - 5.0
            );

            if (map.isLayoutOccurred()) {
                map.zoomToBoundingBox(boundingBox, false);
            } else {
                map.addOnFirstLayoutListener((v, left, top, right, bottom) -> map.zoomToBoundingBox(boundingBox, false));
            }
        });

        return root;
    }

    @Override
    public Optional<SwipeRefreshLayout> getSwipeRefreshLayout() {
        return Optional.of(binding.swipeRefresh);
    }

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return getViewLifecycleOwner();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}