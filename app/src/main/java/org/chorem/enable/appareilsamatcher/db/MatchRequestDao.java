/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MatchRequestDao {
    @Query("SELECT * FROM match_requests order by map_date desc")
    LiveData<List<MatchRequest>> getAll();

    @Query("SELECT id FROM match_requests")
    List<Integer> getAllIds();

    @Query("SELECT MAX(map_date) FROM match_requests")
    LiveData<Long> getLastUpate();

    @Insert
    void insertAll(MatchRequest... matchRequest);

    @Query("DELETE FROM match_requests")
    void deleteAll();

}
