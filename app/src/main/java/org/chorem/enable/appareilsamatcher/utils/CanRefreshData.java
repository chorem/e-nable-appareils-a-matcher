/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import org.chorem.enable.appareilsamatcher.R;
import org.chorem.enable.appareilsamatcher.business.FetchDataWorker;

import java.util.Optional;

public interface CanRefreshData {

    Optional<SwipeRefreshLayout> getSwipeRefreshLayout();

    Context getContext();

    LifecycleOwner getLifecycleOwner();

    default void startRefreshing() {
        getSwipeRefreshLayout().ifPresent(l -> l.setRefreshing(true));
    }

    default void stopRefreshing() {
        getSwipeRefreshLayout().ifPresent(l -> l.setRefreshing(false));
    }

    default void refreshData() {
        startRefreshing();
        Data data = new Data.Builder()
                .putBoolean(FetchDataWorker.DATA_OPTION_POST_NOTIFICATION, false)
                .build();
        WorkRequest fetchDataWorkRequest = new OneTimeWorkRequest.Builder(FetchDataWorker.class)
                .setConstraints(ENableUtils.getConstraintsForWorkRequests(false))
                .setInputData(data)
                .build();
        Context context = getContext();
        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueue(fetchDataWorkRequest);
        workManager.getWorkInfoByIdLiveData(fetchDataWorkRequest.getId())
                .observe(getLifecycleOwner(), info -> {
                    if (info != null && info.getState().isFinished()) {
                        stopRefreshing();
                        int newMatchRequests = info.getOutputData()
                                .getInt(FetchDataWorker.DATA_OPTION_NB_NEW_MATCH_REQUESTS, 0);
                        String message = context.getResources()
                                .getQuantityString(R.plurals.new_demands, newMatchRequests, newMatchRequests);
                        Log.i(getClass().getName(), newMatchRequests +" new demands");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
