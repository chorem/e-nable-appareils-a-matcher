/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.db;

import android.text.format.DateUtils;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "match_requests")
public class MatchRequest {

    @PrimaryKey
    protected int id;
    @ColumnInfo(name = "map_date")
    protected long mapDate;
    @ColumnInfo(name = "first_name")
    protected String firstName;
    protected String zip;
    protected String city;
    protected String country;
    protected double latitude;
    protected double longitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getMapDate() {
        return mapDate;
    }

    public void setMapDate(long mapDate) {
        this.mapDate = mapDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        return String.format("%s %s (%s)", zip, city, country);
    }

    public CharSequence getRelativeMapDateSpan() {
        return DateUtils.getRelativeTimeSpanString(mapDate * 1000);
    }
}
