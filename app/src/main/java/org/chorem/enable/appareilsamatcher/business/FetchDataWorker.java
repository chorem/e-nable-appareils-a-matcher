/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.business;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.StatusLine;
import org.chorem.enable.appareilsamatcher.db.EnableDatabase;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.db.MatchRequestDao;
import org.chorem.enable.appareilsamatcher.utils.ENableUtils;
import org.chorem.enable.appareilsamatcher.utils.NotificationUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class FetchDataWorker extends Worker {

    public static final String TAG = "FetchDataWorker";

    public static final String DATA_OPTION_POST_NOTIFICATION = "postNotification";
    public static final String DATA_OPTION_NB_NEW_MATCH_REQUESTS = "nbNewMatchRequests";

    public FetchDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Data.Builder outputBuilder = new Data.Builder();
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(ENableUtils.MATCH_REQUESTS_URL);
            CloseableHttpResponse response = client.execute(request);

            StatusLine statusLine = new StatusLine(response);
            if (statusLine.isSuccessful()) {
                String receivedData = EntityUtils.toString(response.getEntity());

                JSONObject responseData = new JSONObject(receivedData);
                JSONArray appareilsAMatcher = responseData.getJSONObject("data")
                        .getJSONArray("appareilsAMatcher");

                EnableDatabase db = EnableDatabase.getDatabase(getApplicationContext());
                MatchRequestDao dao = db.matchRequestDao();
                List<Integer> allIds = dao.getAllIds();
                Log.d(TAG, allIds.size() + " données en base");
                dao.deleteAll();

                int newMatchRequests = 0;
                for (int i = 0; i < appareilsAMatcher.length(); i++) {
                    JSONObject appareilAMatcher = appareilsAMatcher.getJSONObject(i);
                    MatchRequest matchRequest = createMatchRequest(appareilAMatcher);
                    if (!allIds.contains(matchRequest.getId())) {
                        newMatchRequests++;
                    }
                    dao.insertAll(matchRequest);
                }

                outputBuilder.putInt(DATA_OPTION_NB_NEW_MATCH_REQUESTS, newMatchRequests);
                boolean showNotification = getInputData().getBoolean(DATA_OPTION_POST_NOTIFICATION, true);
                Log.i(TAG, "showNotification " + showNotification);
                if (showNotification && newMatchRequests > 0) {
                    NotificationUtils.postNewMatchRequestsNotification(getApplicationContext(), newMatchRequests);
                }

            } else {
                Log.e(TAG, "Error " + statusLine);
                NotificationUtils.postMatchRequestsFetchFailureNotification(getApplicationContext(), statusLine.toString());
                return Result.failure();
            }

        } catch (IOException | ParseException | JSONException e) {
            Log.e(TAG, "Error " + e.getMessage());
            NotificationUtils.postMatchRequestsFetchFailureNotification(getApplicationContext(), e.getLocalizedMessage());
            return Result.failure();
        }

        return Result.success(outputBuilder.build());

    }

    @NonNull
    private static MatchRequest createMatchRequest(JSONObject appareilAMatcher) throws JSONException {
        MatchRequest matchRequest = new MatchRequest();
        matchRequest.setId(appareilAMatcher.getInt("ID"));
        matchRequest.setMapDate(appareilAMatcher.getLong("map_date"));
        matchRequest.setFirstName(appareilAMatcher.getString("firstname"));
        matchRequest.setZip(appareilAMatcher.getString("zip"));
        matchRequest.setCity(appareilAMatcher.getString("city"));
        matchRequest.setCountry(appareilAMatcher.getString("country"));
        JSONObject coordinates = appareilAMatcher.getJSONObject("coordinates");
        matchRequest.setLatitude(coordinates.getDouble("lat"));
        matchRequest.setLongitude(coordinates.getDouble("lng"));
        return matchRequest;
    }
}