/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.db.MatchRequestRepository;

import java.util.List;

public class MatchRequestsViewModel extends AndroidViewModel {

    protected MatchRequestRepository matchRequestRepository;
    private final LiveData<List<MatchRequest>> allMatchRequests;

    public MatchRequestsViewModel(Application application) {
        super(application);
        matchRequestRepository = new MatchRequestRepository(application);
        allMatchRequests = matchRequestRepository.getAllMatchRequests();
    }

    public LiveData<List<MatchRequest>> getAllMatchRequests() {
        return allMatchRequests;
    }

}