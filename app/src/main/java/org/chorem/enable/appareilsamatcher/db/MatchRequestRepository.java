/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.db;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class MatchRequestRepository {

    protected MatchRequestDao matchRequestDao;
    protected LiveData<List<MatchRequest>> allMatchRequests;

    protected LiveData<Long> lastUpdate;

    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public MatchRequestRepository(Application application) {
        EnableDatabase db = EnableDatabase.getDatabase(application);
        matchRequestDao = db.matchRequestDao();
        allMatchRequests = matchRequestDao.getAll();
        lastUpdate = matchRequestDao.getLastUpate();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<MatchRequest>> getAllMatchRequests() {
        return allMatchRequests;
    }

    public LiveData<Long> getLastUpdate() {
        return lastUpdate;
    }
}
