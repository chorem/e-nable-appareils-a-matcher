/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationChannelCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import org.chorem.enable.appareilsamatcher.ui.MainActivity;
import org.chorem.enable.appareilsamatcher.R;

public class NotificationUtils {

    private static final String E_NABLE_CHANNEL_ID = "e-nable-notif-channel";
    private static final int NEW_MATCH_REQUESTS_NOTIF_ID = 1;
    private static final int ERROR_NOTIF_ID = 2;
    private NotificationUtils() {
        throw new AssertionError();
    }

    public static void createNotificationChannel(@NonNull Context context,
                                                 @NonNull ActivityResultCaller activityResultCaller) {
        doIfCanPostNotification(context, activityResultCaller, () -> createNotificationChannelSafe(context));
    }

    private static void createNotificationChannelSafe(@NonNull Context context) {
        String name = context.getString(R.string.notification_channel_name);
        NotificationChannelCompat channel =
                new NotificationChannelCompat.Builder(E_NABLE_CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT)
                        .setName(name)
                        .build();
        // Register the channel with the system.
        NotificationManagerCompat.from(context).createNotificationChannel(channel);
    }

    public static void postNewMatchRequestsNotification(@NonNull Context context,
                                                        int newMatchRequests) {
        doIfCanPostNotification(context, null, () -> postNewMatchRequestsNotificationSafe(context, newMatchRequests));
    }

    private static void postNewMatchRequestsNotificationSafe(@NonNull Context context, int newMatchRequests) {
        String message = context.getResources().getQuantityString(R.plurals.new_demands, newMatchRequests, newMatchRequests);
        String title = context.getString(R.string.new_demands_title);
        postENableNotificationSafe(context, NEW_MATCH_REQUESTS_NOTIF_ID, title, message);
    }

    public static void postMatchRequestsFetchFailureNotification(Context context, String errorMessage) {
        doIfCanPostNotification(context, null, () -> postMatchRequestsFetchFailureNotificationSafe(context, errorMessage));
    }

    private static void postMatchRequestsFetchFailureNotificationSafe(@NonNull Context context, String errorMessage) {
        postENableNotificationSafe(context, ERROR_NOTIF_ID, context.getString(R.string.fetch_error), errorMessage);
    }

    @SuppressLint("MissingPermission")
    private static void postENableNotificationSafe(Context context, int notifId, String title, String text) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        String channelName = context.getString(R.string.notification_channel_name);
        Notification notification = new NotificationCompat.Builder(context, channelName)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_hand_notif)
                .setTicker("e-Nable")
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setChannelId(E_NABLE_CHANNEL_ID)
                .setCategory(notifId == ERROR_NOTIF_ID ? Notification.CATEGORY_ERROR : Notification.CATEGORY_SERVICE)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notifId, notification);
    }

    private static void doIfCanPostNotification(@NonNull Context context,
                                                @Nullable ActivityResultCaller activityResultCaller,
                                                @NonNull Runnable doIfGranted) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        if (!notificationManager.areNotificationsEnabled()
                && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
            int notificationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.POST_NOTIFICATIONS);
            if (notificationPermission == PackageManager.PERMISSION_GRANTED) {
                doIfGranted.run();

            } else if (activityResultCaller != null) {
                ActivityResultLauncher<String> notificationPermissionRequest =
                    activityResultCaller.registerForActivityResult(
                        new ActivityResultContracts.RequestPermission(),
                        result -> {
                            if (result) {
                                doIfGranted.run();
                            }
                        }
                    );
                notificationPermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        } else {
            doIfGranted.run();
        }
    }
}
