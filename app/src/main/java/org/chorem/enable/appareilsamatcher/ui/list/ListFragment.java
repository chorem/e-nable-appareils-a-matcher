/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui.list;

import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.chorem.enable.appareilsamatcher.databinding.FragmentListBinding;
import org.chorem.enable.appareilsamatcher.utils.CanRefreshData;
import org.chorem.enable.appareilsamatcher.utils.LocationUtils;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.ui.MatchRequestsViewModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * A fragment representing a list of Items.
 */
public class ListFragment extends Fragment implements CanRefreshData {

    private MatchRequestRecyclerViewAdapter adapter;
    private LiveData<List<MatchRequest>> allMatchRequests;
    private Location currentLocation;
    private FragmentListBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        SwipeRefreshLayout swipeRefreshLayout = binding.swipeRefresh;
        swipeRefreshLayout.setOnRefreshListener(this::refreshData);

        // Set the adapter
        MatchRequestsViewModel listViewModel =
                new ViewModelProvider(this).get(MatchRequestsViewModel.class);
        adapter = new MatchRequestRecyclerViewAdapter(listViewModel.getAllMatchRequests().getValue());
        binding.list.setAdapter(adapter);
        allMatchRequests = listViewModel.getAllMatchRequests();
        allMatchRequests.observe(getViewLifecycleOwner(), this::updateAdapterMatchRequests);

        //if (true)  throw new RuntimeException("test");
        /*
        TODO see if we really need this
        displayProgressDialog();
        LocationUtils.requestCurrentLocation(context, this, location -> {
            currentLocation = location;
            updateAdapterMatchRequests(allMatchRequests.getValue());
            hideProgressDialog();
        });
        */

        return root;
    }

    private void updateAdapterMatchRequests(List<MatchRequest> matchRequests) {
        if (matchRequests != null && currentLocation != null) {
            double currentLatitude = currentLocation.getLatitude();
            double currentLongitude = currentLocation.getLongitude();
            matchRequests = matchRequests.stream()
                    .sorted(Comparator.comparingDouble(matchRequest -> LocationUtils.distanceToAsDouble(
                                currentLatitude, currentLongitude,
                                matchRequest.getLatitude(), matchRequest.getLongitude()
                            )
                    ))
                    .collect(Collectors.toList());
        }
        adapter.setItems(matchRequests);
    }

    @Override
    public Optional<SwipeRefreshLayout> getSwipeRefreshLayout() {
        return Optional.of(binding.swipeRefresh);
    }

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return getViewLifecycleOwner();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}