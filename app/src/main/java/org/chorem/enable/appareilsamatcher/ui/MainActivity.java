/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.DialogConfiguration;
import org.acra.config.DialogConfigurationBuilder;
import org.acra.config.MailSenderConfiguration;
import org.acra.config.MailSenderConfigurationBuilder;
import org.acra.data.StringFormat;
import org.chorem.enable.appareilsamatcher.R;
import org.chorem.enable.appareilsamatcher.business.FetchDataWorker;
import org.chorem.enable.appareilsamatcher.databinding.ActivityMainBinding;
import org.chorem.enable.appareilsamatcher.utils.CanRefreshData;
import org.chorem.enable.appareilsamatcher.utils.ENableUtils;
import org.chorem.enable.appareilsamatcher.utils.NotificationUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener, CanRefreshData {

    private static final String TAG = "MainActivity";

    private ActivityMainBinding binding;
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupAcra();

        NotificationUtils.createNotificationChannel(this, this);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);

        setupDrawer();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        setupFetchDataWorker(sharedPreferences, false);
    }

    private void setupDrawer() {
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_map, R.id.nav_list)
                .setOpenableLayout(drawer)
                .build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment_content_main);
        NavController navController = Objects.requireNonNull(navHostFragment).getNavController();
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.findViewById(R.id.webpage_button)
                .setOnClickListener(event -> ENableUtils.openWebUrl(this, "https://e-nable.fr"));
        navigationView.findViewById(R.id.email_button)
                .setOnClickListener(event -> ENableUtils.sendContactEmail(this));
        navigationView.findViewById(R.id.facebook_button)
                .setOnClickListener(event -> ENableUtils.openWebUrl(this, "https://www.facebook.com/enableFrance"));
        navigationView.findViewById(R.id.instagram_button)
                .setOnClickListener(event -> ENableUtils.openWebUrl(this, "https://www.instagram.com/enable_france/"));
        navigationView.findViewById(R.id.gitlab_button)
                .setOnClickListener(event -> ENableUtils.openWebUrl(this, "https://gitlab.nuiton.org/chorem/e-nable-appareils-a-matcher"));
    }

    private void setupFetchDataWorker(SharedPreferences sharedPreferences, boolean replaceEnqueued) {
        boolean sync = sharedPreferences.getBoolean("sync", true);
        if (sync) {
            int frequency = sharedPreferences.getInt("frequency", 6);
            boolean onlyOnWifi = sharedPreferences.getBoolean("onlyOnWifi", false);

            Constraints constraints = ENableUtils.getConstraintsForWorkRequests(onlyOnWifi);

            PeriodicWorkRequest fetchDataWorkRequest =
                    new PeriodicWorkRequest.Builder(FetchDataWorker.class, frequency, TimeUnit.HOURS)
                            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.MINUTES)
                            .setConstraints(constraints)
                            .build();

            WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    FetchDataWorker.TAG,
                    replaceEnqueued ? ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE : ExistingPeriodicWorkPolicy.KEEP,
                    fetchDataWorkRequest
            );

        } else {
            Log.d(TAG, "stop syncing");
            WorkManager.getInstance(this).cancelUniqueWork(FetchDataWorker.TAG);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu instanceof MenuBuilder) {
            ((MenuBuilder) menu).setOptionalIconsVisible(true);
        }
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection.
        if (item.getItemId() == R.id.action_refresh) {
            refreshData();
            return true;
        }
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Optional<SwipeRefreshLayout> getSwipeRefreshLayout() {
        return Optional.ofNullable(findViewById(R.id.swipe_refresh));
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return this;
    }

    private void setupAcra() {
        DialogConfiguration dialogConfig = new DialogConfigurationBuilder()
                //optional, enables the dialog title
                .withTitle(getString(R.string.acra_dialog_title))
                //required
                .withText(getString(R.string.acra_dialog_text))
                //optional, enables the comment input
                .withCommentPrompt(getString(R.string.acra_dialog_comment))
                //optional, enables the email input
                .withEmailPrompt(getString(R.string.acra_dialog_email))
                //defaults to android.R.drawable.ic_dialog_alert
                .withResIcon(R.drawable.ic_error)
                //optional, defaults to @android:style/Theme.Dialog
                .withResTheme(android.R.style.Theme_DeviceDefault_Light_Dialog)
                //allows other customization
                //.withReportDialogClass(MyCustomDialog.class)
                .build();

        MailSenderConfiguration mailSenderConfiguration = new MailSenderConfigurationBuilder()
                //required
                .withMailTo("e-nable-appareils-a-matcher@projet.codelutin.com")
                //defaults to true
                //.withReportAsFile(true)
                //defaults to ACRA-report.stacktrace
                //.withReportFileName("Crash.txt")
                //defaults to "<applicationId> Crash Report"
                //.withSubject(getString(R.string.mail_subject))
                //defaults to empty
                //.withBody(getString(R.string.mail_body))
                .build();

        ACRA.init(getApplication(), new CoreConfigurationBuilder()
                //core configuration:
                .withBuildConfigClass(BuildConfig.class)
                .withReportFormat(StringFormat.JSON)
                .withPluginConfigurations(dialogConfig, mailSenderConfiguration)
        );
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, @Nullable String key) {
        if (key == null) {
            return;
        }
        // use constants
        switch (key) {
            case "sync":
            case "onlyOnWifi":
            case "frequency":
                setupFetchDataWorker(sharedPreferences, true);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}