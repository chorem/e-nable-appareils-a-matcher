/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.NetworkType;

import org.chorem.enable.appareilsamatcher.R;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;

public class ENableUtils {

    private ENableUtils() {
        throw new AssertionError();
    }

    public static final String MATCH_REQUESTS_URL = "https://e-nable.fr/wp/wp-admin/admin-ajax.php?action=getusermarkers";
    public static final String ENABLE_EMAIL_ADDRESS = "contact@e-nable.fr";

    public static void sendRequestMatchingEmail(Context context, MatchRequest matchRequest) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ ENABLE_EMAIL_ADDRESS });
        intent.putExtra(Intent.EXTRA_SUBJECT,
                context.getString(R.string.matching_email_subject, matchRequest.getFirstName()));
        intent.putExtra(Intent.EXTRA_TEXT,
                context.getString(R.string.matching_email_text,
                        matchRequest.getFirstName(),
                        matchRequest.getZip(),
                        matchRequest.getCity(),
                        matchRequest.getCountry()));
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.matching_email_choose_message)));
    }

    public static void sendContactEmail(Context context) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ ENABLE_EMAIL_ADDRESS });
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.matching_email_choose_message)));
    }

    @NonNull
    public static Constraints getConstraintsForWorkRequests(boolean onlyOnWifi) {
        return new Constraints.Builder()
                .setRequiredNetworkType(onlyOnWifi ? NetworkType.NOT_ROAMING : NetworkType.CONNECTED)
                .build();
    }

    public static void openWebUrl(Context context, String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        context.startActivity(intent);
    }
}
