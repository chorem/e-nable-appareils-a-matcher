/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui.map;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.appcompat.content.res.AppCompatResources;

import org.chorem.enable.appareilsamatcher.R;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.utils.ENableUtils;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

import java.util.Objects;

public class MapMarker extends Marker {

    public MapMarker(MapView map, MatchRequest matchRequest) {
        super(map);

        Context context = map.getContext();
        setPosition(new GeoPoint(matchRequest.getLatitude(), matchRequest.getLongitude()));
        Drawable icon = AppCompatResources.getDrawable(context, R.drawable.ic_marker);
        int iconColor = map.getResources().getColor(R.color.button, null);
        Objects.requireNonNull(icon).setTint(iconColor);
        setIcon(icon);
        setInfoWindow(new MarkerInfoWindow(R.layout.map_marker_info_window, map) {
            @Override
            public void onOpen(Object item) {
                super.onOpen(item);
                ImageView emailButton = mView.findViewById(R.id.bubble_action);
                emailButton.setOnClickListener(v ->
                        ENableUtils.sendRequestMatchingEmail(v.getContext(), matchRequest));
            }
        });
        setImage(AppCompatResources.getDrawable(context, R.drawable.ic_hand));
        setTitle(matchRequest.getFirstName());
        setSubDescription(matchRequest.getRelativeMapDateSpan().toString());
        setSnippet(matchRequest.getLocation());
    }
}
