/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.ui.list;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.chorem.enable.appareilsamatcher.databinding.FragmentListItemBinding;
import org.chorem.enable.appareilsamatcher.db.MatchRequest;
import org.chorem.enable.appareilsamatcher.utils.ENableUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MatchRequest}.
 */
public class MatchRequestRecyclerViewAdapter extends RecyclerView.Adapter<MatchRequestRecyclerViewAdapter.ViewHolder> {

    private final List<MatchRequest> matchRequests = new ArrayList<>();

    public MatchRequestRecyclerViewAdapter(List<MatchRequest> matchRequests) {
        if (matchRequests != null) {
            this.matchRequests.addAll(matchRequests);
        }
    }

    public void setItems(Collection<MatchRequest> matchRequests) {
        this.matchRequests.clear();
        if (matchRequests != null) {
            this.matchRequests.addAll(matchRequests);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                FragmentListItemBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false
                )
        );
    }

    @SuppressLint("QueryPermissionsNeeded")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MatchRequest matchRequest = matchRequests.get(position);
        holder.matchRequest = matchRequest;
        holder.firstNameView.setText(matchRequest.getFirstName());
        holder.locationView.setText(matchRequest.getLocation());
        holder.dateView.setText(matchRequest.getRelativeMapDateSpan());

        holder.requestMatchButton.setOnClickListener(v ->
                ENableUtils.sendRequestMatchingEmail(v.getContext(), matchRequest));
    }

    @Override
    public int getItemCount() {
        return matchRequests.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public MatchRequest matchRequest;
        public final TextView firstNameView;
        public final TextView locationView;
        public final TextView dateView;

        public final ImageView requestMatchButton;

        public ViewHolder(FragmentListItemBinding binding) {
            super(binding.getRoot());
            firstNameView = binding.firstname;
            locationView = binding.location;
            dateView = binding.date;
            requestMatchButton = binding.requestMatching;
        }
    }
}