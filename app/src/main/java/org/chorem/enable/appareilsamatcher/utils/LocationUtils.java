/*
 * e-Nable Appareils à matcher
 * Copyright (C) 2023  kmorin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.chorem.enable.appareilsamatcher.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;

import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.location.LocationManagerCompat;
import androidx.core.util.Consumer;

import org.osmdroid.views.util.constants.MathConstants;

public class LocationUtils {

    private LocationUtils() {
        throw new AssertionError();
    }

    public static void requestCurrentLocation(@NonNull Context context,
                                              @NonNull ActivityResultCaller activityResultCaller,
                                              @NonNull Consumer<Location> locationCallback) {
        int permissions = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissions == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation(context, locationCallback);

        } else {
            ActivityResultLauncher<String> locationPermissionRequest = activityResultCaller.registerForActivityResult(
                    new ActivityResultContracts.RequestPermission(),
                    result -> {
                        if (result) {
                            getCurrentLocation(context, locationCallback);
                        }
                    }
            );
            locationPermissionRequest.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
    }

    @SuppressLint("MissingPermission") // called only when we are sure we have the permission
    private static void getCurrentLocation(@NonNull Context context, @NonNull Consumer<Location> locationCallback) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (LocationManagerCompat.isLocationEnabled(locationManager)) {
            boolean hasGps = LocationManagerCompat.hasProvider(locationManager, LocationManager.GPS_PROVIDER);
            boolean hasNetwork = LocationManagerCompat.hasProvider(locationManager, LocationManager.NETWORK_PROVIDER);
            if (hasGps || hasNetwork) {
                //FIXME
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    LocationManagerCompat.getCurrentLocation(
                            locationManager,
                            hasGps ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER,
                            null,
                            context.getMainExecutor(),
                            locationCallback
                    );
                }
            }
        } else {
            locationCallback.accept(null);
        }
    }

    /**
     * @return distance in meters
     * @see <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine formula</a>
     * @see <a href="http://www.movable-type.co.uk/scripts/gis-faq-5.1.html">GIS FAQ</a>
     * @since 6.0.0
     */
    public static double distanceToAsDouble(final double latitude1, final double longitude1,
                                     final double latitude2, final double longitude2) {
        final double lat1 = MathConstants.DEG2RAD * latitude1;
        final double lat2 = MathConstants.DEG2RAD * latitude2;
        final double lon1 = MathConstants.DEG2RAD * longitude1;
        final double lon2 = MathConstants.DEG2RAD * longitude2;
        int RADIUS_EARTH_METERS = 6378137; // http://en.wikipedia.org/wiki/Earth_radius#Equatorial_radius
        return RADIUS_EARTH_METERS * 2 * Math.asin(Math.min(1, Math.sqrt(
                Math.pow(Math.sin((lat2 - lat1) / 2), 2)
                        + Math.cos(lat1) * Math.cos(lat2)
                        * Math.pow(Math.sin((lon2 - lon1) / 2), 2)
        )));
    }
}
