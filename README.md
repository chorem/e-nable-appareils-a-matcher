# e-Nable appareils à matcher

e-Nable est un mouvement mondial regroupant plus de 15 000 bénévoles, dont le but est de créer des appareils d’assistance aux handicaps.

En France, ces bricoleurs que l’on appelle les « makers » se sont regroupés sur cette plateforme de mise en relation et proposent aux personnes sans doigts ou sans poignet de recevoir gratuitement un appareil personnalisé.

## L'appli

Les makers doivent penser à régulièrement aller voir la carte des appareils à matcher (https://e-nable.fr/fr/e-nable/en-france/), or on n'y pense pas toujours. C'est pourquoi cette application a été créée. Elle permet :
- de voir les appareils à matcher sur une carte ou une liste
- de recevoir une notification en cas de nouvelle demande d'appareil
- d'envoyer un mail pour accepter une demande
